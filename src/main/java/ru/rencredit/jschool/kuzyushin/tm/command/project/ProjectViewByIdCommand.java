package ru.rencredit.jschool.kuzyushin.tm.command.project;

import ru.rencredit.jschool.kuzyushin.tm.entity.Project;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class ProjectViewByIdCommand extends AbstractProjectShowCommand {

    @Override
    public String name() {
        return "project-view-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show project by id";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        final String userId = serviceLocator.getAuthService().getUserId();
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findOneById(userId, id);
        if (project == null) {
            System.out.println("[FAILED]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

}
