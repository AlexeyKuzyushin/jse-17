package ru.rencredit.jschool.kuzyushin.tm.command.data.base64;

import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.constant.DataConstant;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import java.io.File;
import java.nio.file.Files;

public final class DataBase64ClearCommand extends AbstractCommand {

    @Override
    public String name() {
        return "data-base64-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove base64 file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE BASE64 FILE]");
        final File file = new File(DataConstant.FILE_BASE64);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
