package ru.rencredit.jschool.kuzyushin.tm.api.service;

import ru.rencredit.jschool.kuzyushin.tm.dto.Domain;

public interface IDomainService {

    void load(Domain domain);

    void export(Domain domain);
}
