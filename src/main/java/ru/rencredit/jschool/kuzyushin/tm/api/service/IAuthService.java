package ru.rencredit.jschool.kuzyushin.tm.api.service;

import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

public interface IAuthService {

    String getUserId();

    boolean isAuth();

    void checkRoles(Role[] roles);

    void login(String login, String password);

    void logout();
}
