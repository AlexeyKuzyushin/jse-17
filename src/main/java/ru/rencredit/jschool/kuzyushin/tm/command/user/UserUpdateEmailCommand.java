package ru.rencredit.jschool.kuzyushin.tm.command.user;

import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class UserUpdateEmailCommand extends AbstractCommand {

    @Override
    public String name() {
        return "user-update-email";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update user e-mail";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER E-MAIL]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findById(id);
        if (user == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("ENTER EMAIL:");
        final String email = TerminalUtil.nextLine();
        final User userUpdated = serviceLocator.getUserService().updateEmail(id, email);
        if (userUpdated == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("[OK]");
    }
}
