package ru.rencredit.jschool.kuzyushin.tm.command.user;

import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public class UserLockCommand extends AbstractCommand {

    @Override
    public String name() {
        return "user-lock";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Lock user";
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("[ENTER LOGIN:]");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockUserByLogin(login);
        System.out.println("[OK]");
    }
}
