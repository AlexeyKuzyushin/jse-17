package ru.rencredit.jschool.kuzyushin.tm.command.data.base64;

import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.constant.DataConstant;
import ru.rencredit.jschool.kuzyushin.tm.dto.Domain;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;
import sun.misc.BASE64Encoder;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;

public final class DataBase64SaveCommand extends AbstractCommand {

    @Override
    public String name() {
        return "data-base64-save";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Save data to base64 file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 SAVE]");

        final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);

        final File file = new File(DataConstant.FILE_BASE64);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();

        final byte[] originalData = byteArrayOutputStream.toByteArray();
        final String encodedData = new BASE64Encoder().encode(originalData);
        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(encodedData.getBytes());
        fileOutputStream.close();

        byteArrayOutputStream.close();

        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
