package ru.rencredit.jschool.kuzyushin.tm.api.repository;

import ru.rencredit.jschool.kuzyushin.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {

    void add(String userId, Task task);

    void add (List<Task> tasks);

    void add (Task... tasks);

    Task add (Task task);

    void remove(String userId, Task task);

    List<Task> findAll(String userId);

    void clear(String userId);

    Task findOneById(String userId, String id);

    Task findOneByName(String userId, String name);

    Task findOneByIndex(String userId, Integer index);

    Task removeById(String userId, String id);

    Task removeByName(String userId, String name);

    Task removeByIndex(String userId, Integer index);

    void load (List<Task> tasks);

    void load (Task... tasks);

    void clear();

    List<Task> findAll();
}
