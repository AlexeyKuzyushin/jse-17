package ru.rencredit.jschool.kuzyushin.tm.command.data.base64;

import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.constant.DataConstant;
import ru.rencredit.jschool.kuzyushin.tm.dto.Domain;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.util.Base64;

public final class DataBase64LoadCommand extends AbstractCommand {

    @Override
    public String name() {
        return "data-base64-load";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Load data from base64 file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BINARY LOAD]");
        final File file = new File(DataConstant.FILE_BASE64);
        final String originalData = new String(Files.readAllBytes(file.toPath()));
        final byte[] decodedData = new BASE64Decoder().decodeBuffer(originalData);
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodedData);
        final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        final Domain domain = (Domain) objectInputStream.readObject();
        serviceLocator.getDomainService().load(domain);
        objectInputStream.close();
        byteArrayInputStream.close();
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
