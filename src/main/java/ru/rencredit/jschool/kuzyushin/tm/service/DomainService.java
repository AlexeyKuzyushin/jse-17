package ru.rencredit.jschool.kuzyushin.tm.service;

import ru.rencredit.jschool.kuzyushin.tm.api.service.*;
import ru.rencredit.jschool.kuzyushin.tm.dto.Domain;

public class DomainService implements IDomainService {

    private final ITaskService taskService;

    private final IProjectService projectService;

    private final IUserService userService;

    private final IAuthService authService;

    public DomainService(final ITaskService taskService,
                         final IProjectService projectService,
                         final IUserService userService,
                         final IAuthService authService) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.userService = userService;
        this.authService = authService;
    }

    @Override
    public void load(final Domain domain) {
        if (domain == null) return;
        userService.load(domain.getUsers());
        projectService.load(domain.getProjects());
        taskService.load(domain.getTasks());
    }

    @Override
    public void export(final Domain domain) {
        if (domain == null) return;
        authService.logout();
        domain.setTasks(taskService.getTaskList());
        domain.setProjects(projectService.getProjectList());
        domain.setUsers(userService.getUserList());
    }
}
