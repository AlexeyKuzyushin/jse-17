package ru.rencredit.jschool.kuzyushin.tm.command.system;

import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;

import java.util.Arrays;
import java.util.List;

public final class ShowArgCommand extends AbstractCommand {

    @Override
    public String name() {
        return "arguments";
    }

    @Override
    public String arg() {
        return "-arg";
    }

    @Override
    public String description() {
        return "Show application arguments";
    }

    @Override
    public void execute() {
        final List<AbstractCommand> commands = serviceLocator.getCommandService().getCommandList();
        for (AbstractCommand command: commands)
            if (command.arg() != null)
                System.out.println(command.arg());
    }
}
