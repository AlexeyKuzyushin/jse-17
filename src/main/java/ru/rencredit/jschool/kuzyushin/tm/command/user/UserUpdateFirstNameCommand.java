package ru.rencredit.jschool.kuzyushin.tm.command.user;

import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class UserUpdateFirstNameCommand extends AbstractCommand {

    @Override
    public String name() {
        return "user-update-first-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update user first name";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER FIRST NAME]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findById(id);
        if (user == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("ENTER FIRST NAME:");
        final String firstName = TerminalUtil.nextLine();
        final User userUpdated = serviceLocator.getUserService().updateFirstName(id, firstName);
        if (userUpdated == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("[OK]");
    }
}
