package ru.rencredit.jschool.kuzyushin.tm.api.repository;

import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandRepository {

    List<AbstractCommand> getCommandList();
}
