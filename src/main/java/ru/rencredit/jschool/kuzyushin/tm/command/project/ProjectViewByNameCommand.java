package ru.rencredit.jschool.kuzyushin.tm.command.project;

import ru.rencredit.jschool.kuzyushin.tm.entity.Project;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class ProjectViewByNameCommand extends AbstractProjectShowCommand {

    @Override
    public String name() {
        return "project-view-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show project by name";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        final String userId = serviceLocator.getAuthService().getUserId();
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findOneByName(userId, name);
        if (project == null) {
            System.out.println("[FAILED]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }
}
